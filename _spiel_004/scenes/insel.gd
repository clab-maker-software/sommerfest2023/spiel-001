@tool
extends StaticBody2D


@export_range(0.4,4.8,0.2) var width : float = 4.0:
	set(new_width):
		width = new_width
		$Sprite2D.scale.x = width
		$CollisionShape2D.scale.x = width / 4.0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	if not Engine.is_editor_hint():
		if position.y >= G.cam_pos.y + 250 :
			position.y -= 720

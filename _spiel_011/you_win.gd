extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	$CanvasLayer/RichTextLabel.text = "Prima!\nDu hast gewonnen!\nGesammelte Diamanten: " + str(GameLogic.coins_gathered)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_button_pressed():
	GameLogic.coins_gathered = 0
	GameLogic.current_level = self
	GameLogic.current_level_id = -1
	GameLogic.win_level.emit()
	pass # Replace with function body.

extends Area2D

var door_closed : bool = true

func _ready():
	door_closed = true
	GameLogic.open_door.connect(_on_open_door)

func _on_open_door():
	$AnimatedSprite2D.play("ready")
	door_closed = false


func _on_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	if not door_closed:
		GameLogic.win_level.emit()

extends CharacterBody2D


var SPEED = 150.0
const JUMP_VELOCITY = -530.0
var normal_speed = 150.0
var height = 0
var direction = 1

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")


func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta

	# Handle Jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY
		$AudioStreamPlayer2D.pitch_scale = randf_range(0.8,1.2)
		$AudioStreamPlayer2D.play()
	
	if Input.is_action_pressed("jump"):
		SPEED = normal_speed * 2
	else:
		SPEED = normal_speed
	

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	if direction:
		velocity.x = direction * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)

	move_and_slide()
	
func _process(delta):
	normal_speed += delta * 0.5
	if position.y >= height:
#		$Camera2D.limit_bottom = height 
		height = position.y


func _on_right_hit_box_body_entered(body):
	if body.name == "Player":
		direction *= -1


func _on_left_hit_box_body_entered(body):
	if body.name == "Player":
		direction *= -1


func _on_down_hit_box_body_entered(body):
	if body.name == "Player":
		get_tree().change_scene_to_file("res://scenes/end.tscn")

extends Node2D

var max_height = 600
var player_jump = 999


# Called when the node enters the scene tree for the first time.
func _ready():
	$Camera2D.limit_left = 0
	$Camera2D.limit_right = DisplayServer.screen_get_size().x
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	G.cam_pos = $Player.position
	
func _physics_process(delta):
	if $Player.position.y <= player_jump:
		player_jump = $Player.position.y
	var oldScore = G.score
	G.score = floor((player_jump * -1) / 10) + 57
	if oldScore != G.score:
		$CanvasLayer/RichTextLabel.text = " " + str(G.score)
	$Wände.position.y = player_jump
	if $Player.position.y < max_height:
		max_height = $Player.position.y
		$Camera2D.limit_bottom = max_height + 300

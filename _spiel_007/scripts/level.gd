extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	GameLogic.current_level = self
	GameLogic.level_keys_total = 0
	GameLogic.level_keys_gathered = 0
	for child in get_children():
		if child.name.begins_with("key"):
			GameLogic.level_keys_total += 1
	GameLogic.update_ui.connect(_on_update_ui)
	GameLogic.update_ui.emit()
	GameLogic.player = $player
	GameLogic.level_player_spawn_position = $player.position
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_update_ui():
	$CanvasLayer/Label.text = "Coins: " + str(GameLogic.coins_gathered)
	$CanvasLayer/Label2.text = "Keys: " + str(GameLogic.level_keys_gathered) + " / " + str(GameLogic.level_keys_total)

